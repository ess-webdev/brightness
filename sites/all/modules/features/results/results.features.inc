<?php
/**
 * @file
 * results.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function results_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function results_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function results_node_info() {
  $items = array(
    'progress_update' => array(
      'name' => t('Progress update'),
      'base' => 'node_content',
      'description' => t('Project progress outputs'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'scientific_article' => array(
      'name' => t('Scientific article'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
