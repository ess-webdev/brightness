<?php
/**
 * @file
 * results.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function results_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create progress_update content'.
  $permissions['create progress_update content'] = array(
    'name' => 'create progress_update content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create scientific_article content'.
  $permissions['create scientific_article content'] = array(
    'name' => 'create scientific_article content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any progress_update content'.
  $permissions['delete any progress_update content'] = array(
    'name' => 'delete any progress_update content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any scientific_article content'.
  $permissions['delete any scientific_article content'] = array(
    'name' => 'delete any scientific_article content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own progress_update content'.
  $permissions['delete own progress_update content'] = array(
    'name' => 'delete own progress_update content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own scientific_article content'.
  $permissions['delete own scientific_article content'] = array(
    'name' => 'delete own scientific_article content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any progress_update content'.
  $permissions['edit any progress_update content'] = array(
    'name' => 'edit any progress_update content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any scientific_article content'.
  $permissions['edit any scientific_article content'] = array(
    'name' => 'edit any scientific_article content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own progress_update content'.
  $permissions['edit own progress_update content'] = array(
    'name' => 'edit own progress_update content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own scientific_article content'.
  $permissions['edit own scientific_article content'] = array(
    'name' => 'edit own scientific_article content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
