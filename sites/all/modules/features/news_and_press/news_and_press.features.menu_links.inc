<?php
/**
 * @file
 * news_and_press.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function news_and_press_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_news-and-press:news-and-press.
  $menu_links['main-menu_news-and-press:news-and-press'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'news-and-press',
    'router_path' => 'news-and-press',
    'link_title' => 'News and Press',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_news-and-press:news-and-press',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -47,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('News and Press');

  return $menu_links;
}
