<?php

/**
 * @file
 * news_and_press.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function news_and_press_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'news_full';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'news' => 'news',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-news-block_1' => array(
          'module' => 'views',
          'delta' => 'news-block_1',
          'region' => 'sidebar_second',
          'weight' => '-19',
        ),
        'views-events-block' => array(
          'module' => 'views',
          'delta' => 'events-block',
          'region' => 'sidebar_second',
          'weight' => '-18',
        ),
        'views-news-block_4' => array(
          'module' => 'views',
          'delta' => 'news-block_4',
          'region' => 'sidebar_second',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['news_full'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'news_press';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'news-and-press' => 'news-and-press',
        'news-and-press/*' => 'news-and-press/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-press_contact' => array(
          'module' => 'boxes',
          'delta' => 'press_contact',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['news_press'] = $context;

  return $export;
}
