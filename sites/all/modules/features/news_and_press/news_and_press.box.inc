<?php

/**
 * @file
 * news_and_press.box.inc
 */

/**
 * Implements hook_default_box().
 */
function news_and_press_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'press_contact';
  $box->plugin_key = 'simple';
  $box->title = 'Press contact';
  $box->description = 'Press contact';
  $box->options = array(
    'body' => array(
      'value' => '<p><strong>Rikke Nielsen</strong><br />
Communications Consultant<br />
Danish Big Science Secretariat<br />
&nbsp;<br />
Mobile: +45 72 20 10 93<br />
Email: rnie@dti.dk<br />
www.english.bigscience.dk<br />
&nbsp;<br />
Danish Technological Institute<br />
Gregersensvej 7<br />
DK-2630 Taastrup<br />
Denmark<br />
Tel: +45 72 20 20 00</p>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['press_contact'] = $box;

  return $export;
}
