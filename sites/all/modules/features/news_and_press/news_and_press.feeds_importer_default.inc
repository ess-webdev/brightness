<?php

/**
 * @file
 * news_and_press.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function news_and_press_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'node';
  $feeds_importer->config = array(
    'name' => 'ESS News importer',
    'description' => 'Import nodes from ESS public website',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 1,
        'cache_http_result' => TRUE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExJsonPathLines',
      'config' => array(
        'sources' => array(),
        'context' => array(
          'value' => '',
        ),
        'display_errors' => FALSE,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => FALSE,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'feed_item',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:0',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'jsonpath_parser:1',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'jsonpath_parser:2',
            'target' => 'field_link:url',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'jsonpath_parser:6',
            'target' => 'field_link:title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'jsonpath_parser:3',
            'target' => 'created',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'jsonpath_parser:4',
            'target' => 'field_ess_news_image:uri',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'jsonpath_parser:5',
            'target' => 'body',
            'unique' => FALSE,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => '1',
        'authorize' => 1,
        'update_non_existent' => 'skip',
        'skip_hash_check' => 0,
        'insert_new' => 1,
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '3600',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['node'] = $feeds_importer;

  return $export;
}
