<?php

/**
 * @file
 * news_and_press.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function news_and_press_default_rules_configuration() {
  $items = array();
  $items['rules_close_commenting_for_non_in_kind_news'] = entity_import('rules_config', '{ "rules_close_commenting_for_non_in_kind_news" : {
      "LABEL" : "Close commenting for non-in-kind news",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_presave--news" : { "bundle" : "news" } },
      "IF" : [
        { "NOT list_contains" : { "list" : [ "node:field-tags" ], "item" : "1" } }
      ],
      "DO" : [ { "data_set" : { "data" : [ "node:comment" ], "value" : "0" } } ]
    }
  }');
  $items['rules_notify_in_kind_when_comment_is_posted'] = entity_import('rules_config', '{ "rules_notify_in_kind_when_comment_is_posted" : {
      "LABEL" : "Notify In-Kind when comment is posted",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "comment" ],
      "ON" : { "comment_insert--comment_node_news" : { "bundle" : "comment_node_news" } },
      "DO" : [
        { "mail" : {
            "to" : "best.practices@esss.se",
            "subject" : "New comment on BrightnESS website",
            "message" : "A new comment has been posted by on news related to In-Kind activities on the BrightnESS website.\\r\\n\\r\\nNews article: [comment:node]\\r\\nCommenter: [comment:name]\\r\\n\\r\\nView comment here: [comment:url]",
            "from" : "webmaster@esss.se",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_open_commenting_for_in_kind_articles'] = entity_import('rules_config', '{ "rules_open_commenting_for_in_kind_articles" : {
      "LABEL" : "Open commenting for in-kind news",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_presave--news" : { "bundle" : "news" } },
      "IF" : [ { "list_contains" : { "list" : [ "node:field-tags" ], "item" : "1" } } ],
      "DO" : [ { "data_set" : { "data" : [ "node:comment" ], "value" : "2" } } ]
    }
  }');
  return $items;
}
