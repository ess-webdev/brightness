<?php

/**
 * @file
 * partners_people.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function partners_people_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function partners_people_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function partners_people_image_default_styles() {
  $styles = array();

  // Exported image style: profile_picture.
  $styles['profile_picture'] = array(
    'label' => 'Profile picture (100x150)',
    'effects' => array(
      10 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 120,
          'height' => 160,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: small_logo.
  $styles['small_logo'] = array(
    'label' => 'Small logo (120x120)',
    'effects' => array(
      9 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 200,
          'height' => 200,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function partners_people_node_info() {
  $items = array(
    'partner' => array(
      'name' => t('Partner'),
      'base' => 'node_content',
      'description' => t('Partners in the BrightnESS project'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'profile' => array(
      'name' => t('People'),
      'base' => 'node_content',
      'description' => t('Profiles of researchers and scientists in the BrightnESS project'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
