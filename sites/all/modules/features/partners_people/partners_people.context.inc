<?php

/**
 * @file
 * partners_people.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function partners_people_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'partner_full';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'partner' => 'partner',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-profiles-block_1' => array(
          'module' => 'views',
          'delta' => 'profiles-block_1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-news-block_5' => array(
          'module' => 'views',
          'delta' => 'news-block_5',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['partner_full'] = $context;

  return $export;
}
