<?php

/**
 * @file
 * work_packages_and_deliverables.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function work_packages_and_deliverables_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create deliverable content'.
  $permissions['create deliverable content'] = array(
    'name' => 'create deliverable content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create work_package content'.
  $permissions['create work_package content'] = array(
    'name' => 'create work_package content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any deliverable content'.
  $permissions['delete any deliverable content'] = array(
    'name' => 'delete any deliverable content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any work_package content'.
  $permissions['delete any work_package content'] = array(
    'name' => 'delete any work_package content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own deliverable content'.
  $permissions['delete own deliverable content'] = array(
    'name' => 'delete own deliverable content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own work_package content'.
  $permissions['delete own work_package content'] = array(
    'name' => 'delete own work_package content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any deliverable content'.
  $permissions['edit any deliverable content'] = array(
    'name' => 'edit any deliverable content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any work_package content'.
  $permissions['edit any work_package content'] = array(
    'name' => 'edit any work_package content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own deliverable content'.
  $permissions['edit own deliverable content'] = array(
    'name' => 'edit own deliverable content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own work_package content'.
  $permissions['edit own work_package content'] = array(
    'name' => 'edit own work_package content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
