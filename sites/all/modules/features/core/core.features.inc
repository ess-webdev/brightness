<?php

/**
 * @file
 * core.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function core_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
  if ($module == "captcha" && $api == "captcha") {
    return array("version" => "1");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function core_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_date_custom_date_formats().
 */
function core_fe_date_custom_date_formats() {
  $custom_date_formats = array();
  $custom_date_formats['M j, Y'] = 'M j, Y';
  $custom_date_formats['l, j F Y, H:i'] = 'l, j F Y, H:i';
  return $custom_date_formats;
}

/**
 * Implements hook_date_format_types().
 */
function core_date_format_types() {
  $format_types = array();
  // Exported date format type: full
  $format_types['full'] = 'Full';
  // Exported date format type: meta
  $format_types['meta'] = 'Meta';
  return $format_types;
}

/**
 * Implements hook_image_default_styles().
 */
function core_image_default_styles() {
  $styles = array();

  // Exported image style: ic_full_width.
  $styles['ic_full_width'] = array(
    'label' => '(IC) Full width (1200px)',
    'effects' => array(
      13 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'upscale' => 0,
          'respectminimum' => 0,
          'onlyscaleifcrop' => 1,
          'style_name' => 'ic_full_width',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: ic_medium__600_wide.
  $styles['ic_medium__600_wide'] = array(
    'label' => '(IC) Medium (600 wide)',
    'effects' => array(
      9 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 600,
          'height' => 400,
          'upscale' => 0,
          'respectminimum' => 0,
          'onlyscaleifcrop' => 1,
          'style_name' => 'ic_medium__600_wide',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: ic_slide.
  $styles['ic_slide'] = array(
    'label' => '(IC) Slide',
    'effects' => array(
      15 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 600,
          'height' => 450,
          'upscale' => 0,
          'respectminimum' => 0,
          'onlyscaleifcrop' => 0,
          'style_name' => 'ic_slide',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: large__1200x1200_.
  $styles['large__1200x1200_'] = array(
    'label' => 'Large (1200x1200)',
    'effects' => array(
      12 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1200,
          'height' => 1200,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: medium__600_wide_.
  $styles['medium__600_wide_'] = array(
    'label' => 'Medium (600 wide)',
    'effects' => array(
      11 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 400,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: slide.
  $styles['slide'] = array(
    'label' => 'Slide (600x450)',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 450,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: small_square_thumbnail.
  $styles['small_square_thumbnail'] = array(
    'label' => 'Small square thumbnail (80x80)',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 80,
          'height' => 80,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: thumbnail_rectangle.
  $styles['thumbnail_rectangle'] = array(
    'label' => 'Large square thumbnail (120x120)',
    'effects' => array(
      5 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 120,
          'height' => 120,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function core_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
