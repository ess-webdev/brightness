<?php

/**
 * @file
 * core.features.contact_categories.inc
 */

/**
 * Implements hook_contact_categories_defaults().
 */
function core_contact_categories_defaults() {
  return array(
    'General enquiries' => array(
      'category' => 'General enquiries',
      'recipients' => 'brightness@esss.se',
      'reply' => '',
      'weight' => 0,
      'selected' => 1,
    ),
    'Website feedback' => array(
      'category' => 'Website feedback',
      'recipients' => 'webmaster@esss.se',
      'reply' => '',
      'weight' => 0,
      'selected' => 0,
    ),
  );
}
