<?php

/**
 * @file
 * core.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function core_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu_block-2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 2,
    'module' => 'menu_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'esssource' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'esssource',
        'weight' => -8,
      ),
      'esssource_old' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'esssource_old',
        'weight' => -8,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => -8,
      ),
    ),
    'title' => 'Main menu override',
    'visibility' => 0,
  );

  $export['menu_block-4'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 4,
    'module' => 'menu_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'esssource' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'esssource',
        'weight' => -21,
      ),
      'esssource_old' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'esssource_old',
        'weight' => -21,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 2,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
