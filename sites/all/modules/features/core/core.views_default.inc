<?php

/**
 * @file
 * core.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function core_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'ess_news_live';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'json';
  $view->human_name = 'ESS News Live';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'ESS News';
  $handler->display->display_options['use_more_always'] = TRUE;
  $handler->display->display_options['use_more_text'] = 'See all';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['json_file'] = 'https://europeanspallationsource.se/export/external-news';
  $handler->display->display_options['query']['options']['row_apath'] = 'data';
  $handler->display->display_options['query']['options']['enable_pagination_query_parameters'] = 0;
  $handler->display->display_options['query']['options']['show_errors'] = 1;
  $handler->display->display_options['query']['options']['ssl_allow_self_signed'] = 0;
  $handler->display->display_options['query']['options']['ssl_cafile'] = '';
  $handler->display->display_options['query']['options']['ssl_capath'] = '';
  $handler->display->display_options['query']['options']['ssl_local_cert'] = '';
  $handler->display->display_options['query']['options']['ssl_passphrase'] = '';
  $handler->display->display_options['query']['options']['ssl_CN_match'] = '';
  $handler->display->display_options['query']['options']['ssl_verify_depth'] = '';
  $handler->display->display_options['query']['options']['ssl_SNI_enabled'] = 0;
  $handler->display->display_options['query']['options']['ssl_SNI_server_name'] = '';
  $handler->display->display_options['query']['options']['ssl_peer_fingerprint'] = '';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<center><a href="https://europeanspallationsource.se/articles">See All</a></center>';
  $handler->display->display_options['footer']['area']['format'] = 'filtered_html';
  /* Field: path: value */
  $handler->display->display_options['fields']['value_2']['id'] = 'value_2';
  $handler->display->display_options['fields']['value_2']['table'] = 'json';
  $handler->display->display_options['fields']['value_2']['field'] = 'value';
  $handler->display->display_options['fields']['value_2']['label'] = '';
  $handler->display->display_options['fields']['value_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['value_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['value_2']['key'] = 'path';
  /* Field: title: value */
  $handler->display->display_options['fields']['value_1']['id'] = 'value_1';
  $handler->display->display_options['fields']['value_1']['table'] = 'json';
  $handler->display->display_options['fields']['value_1']['field'] = 'value';
  $handler->display->display_options['fields']['value_1']['label'] = '';
  $handler->display->display_options['fields']['value_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['value_1']['alter']['path'] = '[value_2]';
  $handler->display->display_options['fields']['value_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['value_1']['key'] = 'title';
  /* Field: image: value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'json';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['label'] = '';
  $handler->display->display_options['fields']['value']['exclude'] = TRUE;
  $handler->display->display_options['fields']['value']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['value']['empty'] = 'https://europeanspallationsource.se/themes/custom/ess/logo.svg';
  $handler->display->display_options['fields']['value']['key'] = 'image';
  /* Field: created: value */
  $handler->display->display_options['fields']['value_3']['id'] = 'value_3';
  $handler->display->display_options['fields']['value_3']['table'] = 'json';
  $handler->display->display_options['fields']['value_3']['field'] = 'value';
  $handler->display->display_options['fields']['value_3']['label'] = '';
  $handler->display->display_options['fields']['value_3']['exclude'] = TRUE;
  $handler->display->display_options['fields']['value_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['value_3']['key'] = 'created';
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = '';
  $handler->display->display_options['fields']['php']['exclude'] = TRUE;
  $handler->display->display_options['fields']['php']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_output'] = ' <?php 
echo date(\'M d, Y\', $row->value_3);
?>';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="node node-feed-item node-promoted contextual-links-region view-mode-listing feed-item-listing  contextual-links-region">
      <div class="field field-name-image-external-link- feed-item-listing-image">
          <p>
              <a href="[value_2]" target="_blank">
                  <img  src="[value]" width="80" height="80">
              </a>
          </p>
      </div>
      <div class="field field-name-field-link feed-item-listing-title">
          <p><a href="[value_2]" target="_blank">[value_1]</a></p>
      </div>
          <div class="field field-name-post-date feed-item-listing-postdate">
              [php]
          </div>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['ess_news_live'] = $view;

  return $export;
}
