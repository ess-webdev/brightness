<?php

/**
 * @file
 * core.box.inc
 */

/**
 * Implements hook_default_box().
 */
function core_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'brightness_footer_logo';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'BrightnESS footer logo';
  $box->options = array(
    'body' => array(
      'value' => '<div class="footer-logo"><img src="/sites/default/files/brightness.png">is funded by the European Union Framework Programme for Research and Innovation Horizon 2020, under grant agreement 676548.<br>Editor-in-chief: Roger Eriksson.</div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['brightness_footer_logo'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'eu_flag';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'EU flag header';
  $box->options = array(
    'body' => array(
      'value' => '<div class="flag-container-header"><img src="/sites/default/files/eu.jpg"></div>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'flag-container-header',
  );
  $export['eu_flag'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'eu_flag_footer';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'EU flag footer';
  $box->options = array(
    'body' => array(
      'value' => '<div class="flag-container-footer"><div class="flag-container-footer-helper"><img src="/sites/default/files/eu.jpg" /></div></div>
',
      'format' => 'full_html',
    ),
    'additional_classes' => 'flag-container-footer',
  );
  $export['eu_flag_footer'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'footer_logo';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'BrightnESS footer logo New';
  $box->options = array(
    'body' => array(
      'value' => '<div class="footer-logo">BrightnESS² is funded by the European Framework Programme for Research and Innovation Horizon 2020, under grant agreement 823867.</div>',
      'format' => 'full_html',
    ),
  );
  $export['footer_logo'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'partners_area';
  $box->plugin_key = 'simple';
  $box->title = 'Partners area';
  $box->description = 'Partners area';
  $box->options = array(
    'body' => array(
      'value' => '<div class="partners-area-title">Partners Area</div>
<div class="partners-area-link"><a href="https://ess-ics.atlassian.net/wiki/display/BrightnESS" target="_blank">Project Wiki</a></div>
<div class="partners-area-link"><a href="https://ess-ics.atlassian.net/secure/Dashboard.jspa?selectPageId=12805" target="_blank">Project Management Dashboard</a></div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['partners_area'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'twitter_feed';
  $box->plugin_key = 'simple';
  $box->title = 'BrightnESS² on Twitter';
  $box->description = 'BrightnESS Twitter feed';
  $box->options = array(
    'body' => array(
      'value' => '<a class="twitter-timeline" href="https://twitter.com/brightnesseu" data-widget-id="660069034503880704">Tweets fra @brightnesseu</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['twitter_feed'] = $box;

  return $export;
}
