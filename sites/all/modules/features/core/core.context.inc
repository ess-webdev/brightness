<?php

/**
 * @file
 * core.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function core_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'about-section';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'menu' => array(
      'values' => array(
        'about/deliverables' => 'about/deliverables',
        'about/milestones' => 'about/milestones',
        'node/2' => 'node/2',
        'node/3' => 'node/3',
        'node/4' => 'node/4',
        'node/48' => 'node/48',
      ),
    ),
    'path' => array(
      'values' => array(
        'about/deliverables/*' => 'about/deliverables/*',
        'about/milestones/*' => 'about/milestones/*',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;
  $export['about-section'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'contact';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'contact' => 'contact',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-25',
        ),
        'boxes-contact_intro' => array(
          'module' => 'boxes',
          'delta' => 'contact_intro',
          'region' => 'content',
          'weight' => '-24',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['contact'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'frontpage';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-news-block_2' => array(
          'module' => 'views',
          'delta' => 'news-block_2',
          'region' => 'body_column_left',
          'weight' => '-9',
        ),
        'views-events-block' => array(
          'module' => 'views',
          'delta' => 'events-block',
          'region' => 'body_column_left',
          'weight' => '-8',
        ),
        'boxes-twitter_feed' => array(
          'module' => 'boxes',
          'delta' => 'twitter_feed',
          'region' => 'body_column_right',
          'weight' => '-9',
        ),
        'views-ess_news_live-block' => array(
          'module' => 'views',
          'delta' => 'ess_news_live-block',
          'region' => 'body_column_right',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['frontpage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'global';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
    'theme' => array(
      'values' => array(
        'esssource' => 'esssource',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'search-form' => array(
          'module' => 'search',
          'delta' => 'form',
          'region' => 'header',
          'weight' => '-24',
        ),
        'menu_block-2' => array(
          'module' => 'menu_block',
          'delta' => '2',
          'region' => 'header',
          'weight' => '-23',
        ),
        'system-help' => array(
          'module' => 'system',
          'delta' => 'help',
          'region' => 'help',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'boxes-footer_logo' => array(
          'module' => 'boxes',
          'delta' => 'footer_logo',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'boxes-eu_flag_footer' => array(
          'module' => 'boxes',
          'delta' => 'eu_flag_footer',
          'region' => 'footer',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['global'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'global_old';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
    'theme' => array(
      'values' => array(
        'esssource_old' => 'esssource_old',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'search-form' => array(
          'module' => 'search',
          'delta' => 'form',
          'region' => 'header',
          'weight' => '-24',
        ),
        'menu_block-2' => array(
          'module' => 'menu_block',
          'delta' => '2',
          'region' => 'header',
          'weight' => '-23',
        ),
        'system-help' => array(
          'module' => 'system',
          'delta' => 'help',
          'region' => 'help',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'boxes-brightness_footer_logo' => array(
          'module' => 'boxes',
          'delta' => 'brightness_footer_logo',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'boxes-eu_flag_footer' => array(
          'module' => 'boxes',
          'delta' => 'eu_flag_footer',
          'region' => 'footer',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['global_old'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'not_front';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu_block-4' => array(
          'module' => 'menu_block',
          'delta' => '4',
          'region' => 'sidebar_second',
          'weight' => '-22',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['not_front'] = $context;

  return $export;
}
