<?php
/**
 * @file
 * Default theme implementation that displays a node using the DS 1 column
 * layout.
 *
 * If you'd like to create a different template file for nodes, you should
 * create a new DS layout, and place it in the templates/ds folder, just like
 * this implementation.
 *
 * Have a look at the example_layout folder in the DS folder for guidance.
 */
?>
<div class="<?php print $classes; ?> <?php print $ds_content_classes; ?> contextual-links-region">
  <?php print render($contextual_links); ?>
  <?php print render($content['post_date']); ?>
  <div class="news-imag">
    <?php print render($content['field_image']); ?>
    <div class="img-alt">
        <?php print $content['field_image'][0]['#item']['alt']; ?>
    </div>
  </div>
  <?php print render($content['ield_kicker']); ?>
  <?php print render($content['field_lead']); ?>
  <?php print render($content['body']); ?>
  <?php print render($content['field_attachment']); ?>
  <?php print render($content['field_partners']); ?>
</div>
