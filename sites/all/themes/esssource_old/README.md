# ESS source theme

A starter theme for ESS Drupal site.

### Dependencies
* Grunt (included)
* Sass 3.4
* Grunt CLI
* Bourbon(included)
* Neat(included)
* modernizr(included) - Support for csstransform 2D & 3D, svg touch & includes SHIV.
* selectivizr(included)


### How to compile SCSS

CD into your theme using your Terminal.
(cd Sites/sitename/sites/all/themes/themeName)

Run **grunt watch**

No you can write your own SCSS and when you click save it will automatically compile it to CSS.

The element you want to customize needs it's own .scss file.

## How to change the base color for the theme.

- Go in to templates - system - html.tpl.php
- On the body tag you add the color class you want to use.

The following color classes are available.
- blue
- green
- orange
- purple
- yellow
- brown
- grey
- red

If you want to customize the colors you can do so in the _colors.scss file.
